require "/home/victor/Escritorio/LPP/pract06/lib/bibliogem/Referencia"

Nodo = Struct.new(:valor, :siguiente, :anterior) do
end

class Lista
	attr_reader :n_elementos, :nodo_inicial, :nodo_final
	def initialize
		@n_elementos = 0
		@nodo_inicial = Nodo.new()
		@nodo_final = @nodo_inicial
	end
	def push_inicio(valor)
		if @n_elementos == 0
			@nodo_inicial[:valor]=valor
			@nodo_final = @nodo_inicial
			@n_elementos+=1
		elsif @n_elementos == 1
			aux = Nodo.new(valor, @nodo_final, nil)
			@nodo_inicial = aux
			@nodo_final[:anterior]=@nodo_inicial
			@n_elementos+=1
		elsif @n_elementos >= 1
			aux = Nodo.new(valor, @nodo_final, nil)
			@nodo_inicial[:anterior]=aux
			@nodo_inicial=aux
			@n_elementos+=1
		end
	end
	def push_final(valor)
		if(@n_elementos == 0)
			@nodo_inicial[:valor]=valor
			@nodo_final = @nodo_inicial
			@n_elementos+=1
		elsif(@n_elementos == 1)
			aux = Nodo.new(valor, nil, @nodo_inicial)
			@nodo_inicial.set_siguiente(aux)
			@nodo_final = aux
			@n_elementos+=1
		elsif(@n_elementos >= 1)
			aux = Nodo.new(valor,nil,@nodo_final)
			@nodo_final[:siguiente]=aux
			@nodo_final=aux
			@n_elementos+=1
		end
	end
	def insert(pos, valor)
		if(pos < @n_elementos)
			aux = @nodo_inicial
			for i in 0..pos
				aux=aux[:siguiente]
			end
			insert = Nodo.new(valor, aux[:siguiente], aux)
			aux2=aux[:siguiente]
			aux2[:anterior]=insert
			aux[:siguiente]=insert
			@n_elementos+=1
		else
			puts "No hay tantos elementos."
		end
	end
	def delete(pos)
		if(pos < @n_elementos)
			aux = @nodo_inicial
			for i in 0..(pos-1)
				aux=aux[:siguiente]
			end
			delete = aux[:siguiente]
			aux2=delete[:siguiente]
			aux2[:anterior]=aux
			aux[:siguiente]=aux2
			@n_elementos-=1
			return delete
		else
			puts "No hay tantos elementos."
			return nil
		end
	end
	def pop_inicio
		if(@n_elementos == 0)
			puts "La lista esta vacia"
			return nil
		elsif(@n_elementos == 1)
			ret = Nodo.new(@nodo_inicial[:valor], nil, nil)
			@nodo_inicial[:valor]=nil
			@nodo_final = @nodo_inicial
			@n_elementos-=1
			return ret
		elsif(@n_elementos > 1)
			ret = Nodo.new(@nodo_inicial[:valor], @nodo_inicial[:siguiente], nil)
			aux=@nodo_inicial
			@nodo_inicial=@nodo_inicial[:siguiente]
			@nodo_inicial[:anterior]=nil
			aux[:siguiente]=nil
			@n_elementos-=1
			return ret
		end
	end
	def pop_final
		if(@n_elementos == 0)
			puts "La lista esta vacia"
			return nil
		elsif(@n_elementos == 1)
			ret = Nodo.new(@nodo_inicial[:valor], nil, nil)
			@nodo_incial[:valor]=nil
			@nodo_final = @nodo_inicial
			@n_elementos-=1
			return ret
		elsif(@n_elementos > 1)
			ret = Nodo.new(@nodo_final[:valor], nil, @nodo_final[:anterior])
			aux=@nodo_final
			@nodo_final=@nodo_final[:anterior]
			@nodo_final[:siguiente]=nil
			aux[:anterior]=nil
			@n_elementos-=1
			return ret
		end
	end
end
