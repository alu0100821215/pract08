require 'spec_helper'

describe Bibliogem do

	describe Nodo do
		refPrueba = Referencia.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312])
		nodoPrueba = Nodo.new(refPrueba, nil)
		it "Los nodos se crean correctamente" do
			nodoPrueba.should be_an_instance_of Nodo
		end
		it "Se ha introducido correctamente la referencia" do
			nodoPrueba[:valor].should be_an_instance_of Referencia
		end
	end
	
	describe Lista do
		refPrueba = Referencia.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312])
		nodoPrueba = Nodo.new(refPrueba.clone,nil)
		listaPrueba = Lista.new()
		listaPrueba.push_inicio(nodoPrueba.clone)
		it "Se puede insertar un elemento" do
			expect(listaPrueba.n_elementos).to be > 0
		end
		it "Se extrae el primer elemento de la lista" do
			listaPrueba.pop_inicio.should be_an_instance_of Nodo
		end
		listaPrueba2 = Lista.new()
		listaPrueba2.push_inicio(nodoPrueba.clone)
		listaPrueba2.push_inicio(nodoPrueba.clone)
		it "Se pueden insertar varios elementos" do
			expect(listaPrueba2.n_elementos).to be >= 2
		end
		aux=listaPrueba2.nodo_final
		it "Se detecta el nodo anterior correctaente" do
			aux[:anterior].should equal(listaPrueba2.nodo_inicial)
		end
		it "Debe existir una lista con su cabeza" do
			listaPrueba.nodo_inicial.should be_an_instance_of Nodo
		end
	end
	describe Revista do
		it "Revista debe ser derivada de Periodico" do
			Revista.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312]).should be_kind_of(Periodico)
		end
		it "Revista debe ser instancia de Revista" do
			Revista.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312]).should be_an_instance_of(Revista)
		end
	end
	describe Periodico do
		it "Periodico debe ser derivada de Referencia" do
			Periodico.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312]).should be_kind_of(Referencia)
		end
		it "Periodico debe ser instancia de Periodico" do
			Periodico.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312]).should be_an_instance_of(Periodico)
		end
	end
	describe Articulo do
		it "Articulo debe ser derivada de Periodico" do
			Articulo.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312]).should be_kind_of(Periodico)
		end
		it "Articulo debe ser instancia de Articulo" do
			Articulo.new(["Alan"],"El pedrolo",nil,"Indie", 234342, "01-01-2001", [123213],[12321312]).should be_an_instance_of(Articulo)
		end
	end
	# autores, titulo, serie, editorial, nedicion, fecha, isbn10, isbn13
end
